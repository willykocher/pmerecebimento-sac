package br.gov.suframa.pme.recebimentosac.job.task;

import java.util.TimerTask;

import javax.inject.Inject;

import br.gov.suframa.pme.recebimentosac.facade.controller.InformacaoBasicaController;

public class GeraDebitoTask extends TimerTask {

    @Inject
    private InformacaoBasicaController informacaoBasicaController;

    public GeraDebitoTask() {
        super();
    }

    @Override
    public void run() {
        informacaoBasicaController.verificarPLIPendenteDebito();
    }

}

package br.gov.suframa.pme.recebimentosac.listener;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import br.gov.suframa.pme.recebimentosac.job.JobTask;

public class ApplicationListener implements ServletContextListener {

    @Inject
    private JobTask job;

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
        //job.stopTasks();
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
        job.startTasks();
    }

}

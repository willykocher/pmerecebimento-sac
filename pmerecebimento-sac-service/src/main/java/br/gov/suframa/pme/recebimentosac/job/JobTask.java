package br.gov.suframa.pme.recebimentosac.job;

import java.util.Timer;

import javax.inject.Inject;

import br.gov.suframa.pme.recebimentosac.job.task.GeraDebitoTask;

public class JobTask {

    public JobTask() {
        // TODO Auto-generated constructor stub
    }

    private Timer timer = new Timer();

    @Inject
    private GeraDebitoTask geraDebitoTask;

    public void startTasks() {
        timer.scheduleAtFixedRate(geraDebitoTask, 0, 120000);//3600000 = 60 minutos
    }

    public void stopTasks() {
        timer.cancel();
    }
}

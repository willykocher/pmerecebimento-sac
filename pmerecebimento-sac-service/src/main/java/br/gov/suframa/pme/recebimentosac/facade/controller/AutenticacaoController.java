package br.gov.suframa.pme.recebimentosac.facade.controller;

import javax.ejb.EJB;

import br.gov.frameworkdemoiselle.stereotype.FacadeController;
import br.gov.suframa.pme.recebimentosac.business.AutenticacaoNegocio;

@FacadeController
public class AutenticacaoController {

    @EJB
    private AutenticacaoNegocio autenticacaoNegocio;

    public boolean autenticar(String usuario, String senha) {

        return autenticacaoNegocio.autenticar(usuario, senha);

    }

}

package br.gov.suframa.pme.recebimentosac.facade.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.inject.Inject;

import br.gov.frameworkdemoiselle.stereotype.FacadeController;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.gov.suframa.pme.recebimentosac.business.CambioNegocio;
import br.gov.suframa.pme.recebimentosac.business.EnvioLogNegocio;
import br.gov.suframa.pme.recebimentosac.business.GrupoBeneficioNegocio;
import br.gov.suframa.pme.recebimentosac.business.InformacaoBasicaNegocio;
import br.gov.suframa.pme.recebimentosac.business.ItemNegocio;
import br.gov.suframa.pme.recebimentosac.business.MercadoriaNegocio;
import br.gov.suframa.pme.recebimentosac.business.MoedaNegocio;
import br.gov.suframa.pme.recebimentosac.business.PLINegocio;
import br.gov.suframa.pme.recebimentosac.model.Cambio;
import br.gov.suframa.pme.recebimentosac.model.EnvioLog;
import br.gov.suframa.pme.recebimentosac.model.InformacaoBasica;
import br.gov.suframa.pme.recebimentosac.model.Item;
import br.gov.suframa.pme.recebimentosac.model.Mercadoria;
import br.gov.suframa.pme.recebimentosac.model.Moeda;
import br.gov.suframa.pme.recebimentosac.model.PedidoLicenciamentoImportacao;
import br.gov.suframa.pme.recebimentosac.util.HttpClientRequest;
import br.gov.suframa.pme.recebimentosac.vo.DebitoEntradaVO;
import br.gov.suframa.pme.recebimentosac.vo.ExtratoDebitoVO;
import br.gov.suframa.pme.recebimentosac.vo.GerarDebitoManualVO;
import br.gov.suframa.pme.recebimentosac.vo.ItemVO;
import br.gov.suframa.pme.recebimentosac.vo.NCMVO;
import br.gov.suframa.pme.recebimentosac.vo.TipoDebitoRetornoVO;
import br.gov.suframa.poc.cdi.annotation.SystemProperty;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@FacadeController
public class InformacaoBasicaController {

    private static final Long ERRO_ENVIO = 3L;
    private static final String HTTP_ERROR = "Failed : HTTP error code : ";
    private static final String ISENCAO_NAO = "N";
    private static final String ISENCAO_SIM = "S";
    private static final int LOG_ERRO_COMUNICACAO = 2;

    private static final long PROTOCOLO_STATUS_AGUARDANDO_PAGAMENTO = 3;
    private static final long PROTOCOLO_STATUS_LIBERADO_PROCESSAMENTO = 0;

    private static final long STATUS_DEBITO_ERRO_ENVIO = 3L;

    private static final long PROTOCOLO_NOVO = 2L;

    @Inject
    @SystemProperty("pme.sac.url")
    private String url;

    @Inject
    private Logger logger;

    @EJB
    private CambioNegocio cambioNegocio;
    @EJB
    private EnvioLogNegocio envioLogNegocio;
    @EJB
    private GrupoBeneficioNegocio grupoBeneficioNegocio;
    @Inject
    private HttpClientRequest httpClient;
    @EJB
    private InformacaoBasicaNegocio informacaoBasicaNegocio;
    @EJB
    private ItemNegocio itemNegocio;
    @EJB
    private MercadoriaNegocio mercadoriaNegocio;
    @EJB
    private MoedaNegocio moedaNegocio;
    @EJB
    private PLINegocio pliNegocio;

    public List<GerarDebitoManualVO> gerarDebitoManual(Long idPli, String urlArrecadacao) throws JsonProcessingException, IOException {
        List<GerarDebitoManualVO> listJson = new ArrayList<>();
        List<InformacaoBasica> listaInfBasica = informacaoBasicaNegocio.consultarPendenteDebitoPorIdPLI(idPli);

        for (InformacaoBasica informacaoBasica : listaInfBasica) {

            String result = "";

            EnvioLog envioLog = new EnvioLog();

            String json = preparDadosParaSolicitarDebito(informacaoBasica);

            try {
                result = result + solicitarGeracaoDebito(json, urlArrecadacao);

                listJson.add(new GerarDebitoManualVO(json, result));

                ObjectMapper mapper = new ObjectMapper();
                TipoDebitoRetornoVO retorno = mapper.readValue(result, TipoDebitoRetornoVO.class);

                if (result.contains(HTTP_ERROR)) {
                    retorno.setTipoDebitoRetorno(STATUS_DEBITO_ERRO_ENVIO);
                    informacaoBasicaNegocio.atualizarStatusInfoBasica(retorno, informacaoBasica);
                } else {
                    informacaoBasicaNegocio.atualizarStatusInfoBasica(retorno, informacaoBasica);
                }

            } catch (Exception e) {

                envioLog.setInscricaoSuframa(informacaoBasica.getInscricaoSuframa());
                envioLog.setNumeroPLI(informacaoBasica.getNumeroPLI());
                envioLog.setProtocoloEnvio(informacaoBasica.getProtocoloEnvio());
                envioLog.setCodTipoLog(LOG_ERRO_COMUNICACAO);
                envioLog.setMsgLog("Falha na comunicação: " + e.getCause());
                envioLog.setDtCadastro(new Date());

                envioLogNegocio.salvar(envioLog);

                informacaoBasicaNegocio.atualizarStatusInfoBasica(informacaoBasica, ERRO_ENVIO, null);
            }

            logger.log(Level.INFO, json);

        }

        return listJson;
    }

    private String preparDadosParaSolicitarDebito(InformacaoBasica informacaoBasica) throws JsonProcessingException {
        List<NCMVO> listaNCM = new ArrayList<>();
        List<Mercadoria> mercadorias = mercadoriaNegocio.consultarMercadorias(informacaoBasica.getInscricaoSuframa(), informacaoBasica.getNumeroPLI());

        for (Mercadoria mercadoria : mercadorias) {
            List<ItemVO> listaItens = new ArrayList<>();
            Cambio cambio = cambioNegocio.consultarCambio(mercadoria.getCodigoMoedaNegociada(), mercadoria.getCambioDataValidade());
            Moeda moeda = moedaNegocio.consultarMoeda(cambio.getCodigoMoeda());

            List<Item> itens = itemNegocio.consultarItemDeMercadoria(mercadoria.getInscricaoSuframa(), mercadoria.getNumeroPLI(), mercadoria.getCodigoNCM(),
                    mercadoria.getCodigoProduto(), mercadoria.getCodigoTipo(), mercadoria.getCodigoModelo(), mercadoria.getNuMercadoria());

            for (Item item : itens) {
                String isencao = verificarIsencaoItem(mercadoria);
                listaItens.add(new ItemVO(item, isencao));
            }

            listaNCM.add(new NCMVO(mercadoria, moeda.getNomeMoeda(), cambio.getValorFator(), listaItens.size(), listaItens));
        }

        Cambio cambio = cambioNegocio.consultarCambioDolarPorDataValidade(informacaoBasica.getDataEnvio());

        DebitoEntradaVO debitoEntrada = new DebitoEntradaVO(informacaoBasica, new ExtratoDebitoVO(informacaoBasica, cambio.getValorFator().toString(), listaNCM));

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writerWithView(DebitoEntradaVO.class).writeValueAsString(debitoEntrada);
    }

    private String solicitarGeracaoDebito(String jsonObject, String url) {
        return httpClient.post(url, jsonObject);
    }

    private String verificarIsencaoItem(Mercadoria mercadoria) {
        if (mercadoria.getGrupoBeneficio() == 0) {
            return ISENCAO_NAO;
        } else {
            return ISENCAO_SIM;
        }
    }

    @Transactional
    public void verificarPLIPendenteDebito() {
        try {
            List<InformacaoBasica> listaInfBasica = informacaoBasicaNegocio.consultarPendenteDebito();
            Set<Long> set = new HashSet<>();
            for (InformacaoBasica informacaoBasica : listaInfBasica) {
                set.add(informacaoBasica.getProtocoloEnvio());
                String result = "";

                EnvioLog envioLog = new EnvioLog();

                try {

                    result = result + solicitarGeracaoDebito(preparDadosParaSolicitarDebito(informacaoBasica), url);
                    String preparacao = preparDadosParaSolicitarDebito(informacaoBasica);
                    logger.log(Level.INFO, preparacao);
                    logger.log(Level.INFO, result);

                    ObjectMapper mapper = new ObjectMapper();
                    TipoDebitoRetornoVO retorno = mapper.readValue(result, TipoDebitoRetornoVO.class);

                    if (result.contains(HTTP_ERROR)) {
                        retorno.setTipoDebitoRetorno(STATUS_DEBITO_ERRO_ENVIO);
                        informacaoBasicaNegocio.atualizarStatusInfoBasica(retorno, informacaoBasica);
                    } else {
                        informacaoBasicaNegocio.atualizarStatusInfoBasica(retorno, informacaoBasica);
                    }

                } catch (Exception e) {

                    logger.log(Level.WARNING, "Falha de comunicacao", e);

                    envioLog.setInscricaoSuframa(informacaoBasica.getInscricaoSuframa());
                    envioLog.setNumeroPLI(informacaoBasica.getNumeroPLI());
                    envioLog.setProtocoloEnvio(informacaoBasica.getProtocoloEnvio());
                    envioLog.setCodTipoLog(LOG_ERRO_COMUNICACAO);
                    envioLog.setMsgLog("Falha na comunicação: " + result);
                    envioLog.setDtCadastro(new Date());

                    envioLogNegocio.salvar(envioLog);

                }

            }

            Map<Long, Long> listaProtocolos = informacaoBasicaNegocio.verificarStatusPLIParaInfoBasicas(set, PedidoLicenciamentoImportacao.LIBERA_OU_AGUARDA);
            for (Entry<Long, Long> protocoloEnvio : listaProtocolos.entrySet()) {

                if (protocoloEnvio.getValue() == 1L) {
                    pliNegocio.atualizarStatusPLI(protocoloEnvio.getKey(), PROTOCOLO_STATUS_LIBERADO_PROCESSAMENTO, PROTOCOLO_NOVO);
                } else {
                    pliNegocio.atualizarStatusPLI(protocoloEnvio.getKey(), PROTOCOLO_STATUS_AGUARDANDO_PAGAMENTO, PROTOCOLO_NOVO);
                }
            }

        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
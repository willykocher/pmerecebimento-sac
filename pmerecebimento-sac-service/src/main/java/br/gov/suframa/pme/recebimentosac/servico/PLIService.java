package br.gov.suframa.pme.recebimentosac.servico;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.suframa.pme.recebimentosac.facade.controller.InformacaoBasicaController;
import br.gov.suframa.pme.recebimentosac.facade.controller.PLIController;
import br.gov.suframa.pme.recebimentosac.util.DateUtil;
import br.gov.suframa.pme.recebimentosac.vo.EntradaVO;
import br.gov.suframa.pme.recebimentosac.vo.GerarDebitoManualVO;
import br.gov.suframa.pme.recebimentosac.vo.MessageVO;

@Stateless
@Path("pli")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PLIService {

    @Inject
    private PLIController plicontroller;
    @Inject
    private InformacaoBasicaController infoBasicaController;

    private static final String MSG_DADOS_INVALIDOS = "Dados inválidos.";
    private static final String MSG_ERRO_SERVIDOR = "Erro no servidor =>";
    private static final String MSG_BAIXA_DEBITO = "Baixa de débito realizada com sucesso.";
    private static final String MSG_CANCELAMENTO_DEBITO = "Débito cancelado com sucesso.";
    private static final String MSG_NAO_PENDENCIA_DEBITO = "Não foram encontradas pendencia de débito";
    private static final int STATUS_SUCESSO = 0;
    private static final int STATUS_ERRO = 1;

    @PUT
    @Path("baixaDebito")
    public Response atualizarDebito(EntradaVO entrada) {
        try {
            if (entrada.getNumeroSolicitacao() == null)
                return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_DADOS_INVALIDOS)).build();

            plicontroller.atualizarStatusPLINaoProcessado(entrada);

            return Response.ok(new MessageVO(STATUS_SUCESSO, DateUtil.convertFormatBrHora(new Date()), MSG_BAIXA_DEBITO)).build();
        } catch (Exception e) {
            return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_ERRO_SERVIDOR + e.getMessage())).build();
        }
    }

    @PUT
    @Path("cancelarDebito")
    public Response cancelarDebito(EntradaVO entrada) {
        try {
            if (entrada.getNumeroSolicitacao() == null)
                return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_DADOS_INVALIDOS)).build();

            plicontroller.atualizarStatusPLICancelado(entrada);

            return Response.ok(new MessageVO(STATUS_SUCESSO, DateUtil.convertFormatBrHora(new Date()), MSG_CANCELAMENTO_DEBITO)).build();
        } catch (Exception e) {
            return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_ERRO_SERVIDOR + e.getMessage())).build();
        }
    }

    @GET
    @Path("gerarDebitoManual")
    public Response gerarDebitoManual(@QueryParam("idPli") Long idPli, @QueryParam("url") String urlArrecadacao) {
        try {
            if (idPli == null)
                return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_DADOS_INVALIDOS)).build();

            List<GerarDebitoManualVO> list = infoBasicaController.gerarDebitoManual(idPli, urlArrecadacao);

            if (list.size() > 0)
                return Response.ok(list).build();
            else
                return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_NAO_PENDENCIA_DEBITO)).build();
        } catch (Exception e) {
            return Response.serverError().entity(new MessageVO(STATUS_ERRO, DateUtil.convertFormatBrHora(new Date()), MSG_ERRO_SERVIDOR + e.getMessage())).build();
        }
    }
}
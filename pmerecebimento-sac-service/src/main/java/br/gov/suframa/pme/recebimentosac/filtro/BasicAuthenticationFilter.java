package br.gov.suframa.pme.recebimentosac.filtro;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import br.gov.suframa.pme.recebimentosac.facade.controller.AutenticacaoController;
import br.gov.suframa.poc.exception.NegocioException;

/**
 * The Class BasicAuthenticationFilter.
 */
public class BasicAuthenticationFilter implements Filter {

    /** The autenticacao. */
    @Inject
    private AutenticacaoController autenticacao;

    /** The app username. */
    private String appUsername;

    /*
     * (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        appUsername = filterConfig.getInitParameter("appUsername");
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {

            StringTokenizer st = new StringTokenizer(authHeader);
            if (st.hasMoreTokens()) {
                String basic = st.nextToken();

                if ("Basic".equalsIgnoreCase(basic)) {
                    try {
                        String credentials = new String(Base64.decodeBase64(st.nextToken()), "UTF-8");
                        int p = credentials.indexOf(':');
                        if (p != -1) {
                            String username = credentials.substring(0, p).trim();
                            String password = credentials.substring(p + 1).trim();

                            if (!appUsername.equals(username) || !autenticacao.autenticar(username, password)) {
                                unauthorized(response, "Bad credentials");
                            } else {

                                filterChain.doFilter(servletRequest, servletResponse);
                            }
                        } else {
                            unauthorized(response, "Invalid authentication token");
                        }
                    } catch (UnsupportedEncodingException e) {
                        NegocioException.throwExceptionErro("Couldn't retrieve authentication", e);
                    }
                }
            }
        } else
            unauthorized(response);
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy() {
        //not implemented
    }

    /**
     * Unauthorized.
     *
     * @param response the response
     * @param message the message
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void unauthorized(HttpServletResponse response, String message) throws IOException {
        response.setHeader("WWW-Authenticate", "Basic username:password");
        response.sendError(401, message);
    }

    /**
     * Unauthorized.
     *
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void unauthorized(HttpServletResponse response) throws IOException {
        unauthorized(response, "Unauthorized");
    }
}

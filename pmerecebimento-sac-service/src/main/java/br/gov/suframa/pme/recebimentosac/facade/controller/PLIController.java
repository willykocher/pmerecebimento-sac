package br.gov.suframa.pme.recebimentosac.facade.controller;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.inject.Inject;

import br.gov.frameworkdemoiselle.stereotype.FacadeController;
import br.gov.suframa.pme.recebimentosac.business.InformacaoBasicaNegocio;
import br.gov.suframa.pme.recebimentosac.business.PLINegocio;
import br.gov.suframa.pme.recebimentosac.model.PedidoLicenciamentoImportacao;
import br.gov.suframa.pme.recebimentosac.vo.EntradaVO;

@FacadeController
public class PLIController {

    @EJB
    private PLINegocio pliNegocio;
    @EJB
    private InformacaoBasicaNegocio informacaoBasicaNegocio;
    
    @Inject
    private Logger logger;

    private static final long PLI_STATUS_NAO_PROCESSADO = 0;
    private static final long PLI_STATUS_CANCELADO = 4;
    private static final long STATUS_DEBITO_GERADO_PAGO = 4;
    private static final long STATUS_DEBITO_GERADO_CANCELADO = 5;
    private static final Long PROTOCOLO_AGUARDANDO_PAGAMENTO = 3L;

    public PedidoLicenciamentoImportacao atualizarStatusPLINaoProcessado(EntradaVO entrada) {
        //fazer uma mudança de acordo com a regra RN.013
        informacaoBasicaNegocio.atualizarStatusInfoBasica(entrada, STATUS_DEBITO_GERADO_PAGO);

        PedidoLicenciamentoImportacao pli = null;

        Set<Long> set = new HashSet<>();
        set.add(entrada.getNumeroSolicitacao());

        Map<Long, Long> listaProtocolos = informacaoBasicaNegocio.verificarStatusPLIParaInfoBasicas(set, PedidoLicenciamentoImportacao.LIQUIDACAO);
        for (Entry<Long, Long> protocoloEnvio : listaProtocolos.entrySet()) {

            if (protocoloEnvio.getValue() == 1L) {
                pli = pliNegocio.atualizarStatusPLI(entrada.getNumeroSolicitacao(), PLI_STATUS_NAO_PROCESSADO, PROTOCOLO_AGUARDANDO_PAGAMENTO);
            }
        }

        return pli;
    }

    public PedidoLicenciamentoImportacao atualizarStatusPLICancelado(EntradaVO entrada) {
        //fazer uma mudança de acordo com a regra RN.014		
        informacaoBasicaNegocio.atualizarStatusInfoBasica(entrada, STATUS_DEBITO_GERADO_CANCELADO);

        PedidoLicenciamentoImportacao pli = null;

        Set<Long> set = new HashSet<>();
        set.add(entrada.getNumeroSolicitacao());

        Map<Long, Long> listaProtocolos = informacaoBasicaNegocio.verificarStatusPLIParaInfoBasicas(set, PedidoLicenciamentoImportacao.CANCELAMENTO);
        for (Entry<Long, Long> protocoloEnvio : listaProtocolos.entrySet()) {

            if (protocoloEnvio.getValue() == 1L) {
                pli = pliNegocio.atualizarStatusPLI(entrada.getNumeroSolicitacao(), PLI_STATUS_CANCELADO, PROTOCOLO_AGUARDANDO_PAGAMENTO);
            }
        }

        return pli;
    }
}

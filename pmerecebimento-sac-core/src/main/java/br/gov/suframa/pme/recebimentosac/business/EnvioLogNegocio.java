package br.gov.suframa.pme.recebimentosac.business;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.EnvioLog;

@Local
public interface EnvioLogNegocio {
    EnvioLog salvar(EnvioLog envioLog);
}

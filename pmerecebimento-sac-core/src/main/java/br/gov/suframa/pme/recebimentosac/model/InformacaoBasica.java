package br.gov.suframa.pme.recebimentosac.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@IdClass(value = InformacaoBasicaIds.class)
@Table(name = "PMERCB_INFO_BASICAS")
public class InformacaoBasica extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IBA_NU_INS_SUF")
    private Long inscricaoSuframa;

    @Id
    @Column(name = "IBA_NU_PLI")
    private String numeroPLI;

    @Column(name = "IBA_PLI_ID")
    private Long protocoloEnvio;

    @Column(name = "IBA_NU_CNPJ")
    private String cnpjCpf;

    @Column(name = "IBA_NO_EMPRESA")
    private String nomeEmpresa;

    @Column(name = "IBA_TDO_CD")
    private String tipoDocumento;

    @Column(name = "IBA_CD_SERVICO")
    private Long codigoServico;

    @Column(name = "IBA_BFG_ID")
    private Long baseFatoGerador;

    @Column(name = "IBA_VL_BASE_FATO_GERADOR")
    private BigDecimal valorBaseFatoGerador;

    @Column(name = "IBA_DT_ENVIO")
    private Date dataEnvio;

    @Column(name = "IBA_CD_LOCALIDADE", insertable = false, updatable = false)
    private String localidade;

    @Column(name = "IBA_CD_MUNICIPIO")
    private Long codigoMunicipio;

    @Column(name = "IBA_ANO_PROCESSO_PEXPAM")
    private Long numeroProcessoPexpam;

    @Column(name = "IBA_CD_LOCALIDADE")
    private Long anoProcessoPexpam;

    @Column(name = "IBA_VT_MERCADORIA_REAIS")
    private BigDecimal valorTotalMercadoria;

    @Column(name = "IBA_VT_CALCULADO_LIMITADOR_PLI")
    private BigDecimal calculoLimitePLI;

    @Column(name = "IBA_VL_PREVALENCIA_PLI")
    private BigDecimal valorPreviPLI;

    @Column(name = "IBA_VL_PERCENTUAL_REDUCAO_PLI")
    private BigDecimal valorPercentualReducaoPLI;

    @Column(name = "IBA_VL_REDUCAO_PLI")
    private BigDecimal valorReduzidoPLI;

    @Column(name = "IBA_VL_TCIF_PLI")
    private BigDecimal valorTCIF;

    @Column(name = "IBA_VL_REDUCAO_BASE_PLI")
    private BigDecimal valorReduzidoBasePLI;

    @Column(name = "IBA_VT_TCIF_ITENS")
    private BigDecimal valorItensTCIF;

    @Column(name = "IBA_VT_REDUCAO_ITENS")
    private BigDecimal valorTotalReduzidoItens;

    @Column(name = "IBA_VT_REDUCAO_BASE_ITENS")
    private BigDecimal valorTotalReduzidoBaseItens;

    @Column(name = "IBA_VG_TCIF")
    private BigDecimal valorGeralTCIF;

    @Column(name = "IBA_VG_REDUCAO_TCIF")
    private BigDecimal valorGeralReducaoTCIF;

    @Column(name = "IBA_VG_REDUCAO_BASE")
    private BigDecimal valorGeralReduzidoBase;

    @Column(name = "IBA_CONTROLE_COBRANCA_TCIF")
    private Long controleCobrancaTcif;

    @Column(name = "IBA_SAC_NU_DEBITO")
    private Long numeroDebito;

    @Column(name = "IBA_SAC_ANO_DEBITO")
    private Long anoDebito;

    @Column(name = "IBA_SAC_DT_VENCIMENTO")
    private Date dataVencimento;

    @Column(name = "IBA_SDE_ID")
    private Long statusDebito;

    @Column(name = "IBA_SAC_MS_ST_DEBITO")
    private String statusMsgDebito;

    @Column(name = "IBA_DH_CADASTRO")
    private Date dataHoraCadastro;

    @Column(name = "IBA_VL_PERC_LIMITADOR_PLI")
    private BigDecimal percentualLimitadorPLI;

    @Column(name = "IBA_SAC_DT_PAGAMENTO")
    private Date dataDebitoPagamento;

    @Column(name = "IBA_SAC_DT_CANCELAMENTO")
    private Date dataDebitoCancelamento;

    public Date getDataDebitoPagamento() {
        return dataDebitoPagamento;
    }

    public void setDataDebitoPagamento(Date dataDebitoPagamento) {
        this.dataDebitoPagamento = dataDebitoPagamento;
    }

    public Date getDataDebitoCancelamento() {
        return dataDebitoCancelamento;
    }

    public void setDataDebitoCancelamento(Date dataDebitoCancelamento) {
        this.dataDebitoCancelamento = dataDebitoCancelamento;
    }

    public InformacaoBasica() {
        // TODO Auto-generated constructor stub
    }

    public Long getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Long inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    public Long getProtocoloEnvio() {
        return protocoloEnvio;
    }

    public void setProtocoloEnvio(Long protocoloEnvio) {
        this.protocoloEnvio = protocoloEnvio;
    }

    public String getCnpjCpf() {
        return cnpjCpf;
    }

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getCodigoServico() {
        return codigoServico;
    }

    public void setCodigoServico(Long codigoServico) {
        this.codigoServico = codigoServico;
    }

    public Long getBaseFatoGerador() {
        return baseFatoGerador;
    }

    public void setBaseFatoGerador(Long baseFatoGerador) {
        this.baseFatoGerador = baseFatoGerador;
    }

    public BigDecimal getValorBaseFatoGerador() {
        return valorBaseFatoGerador;
    }

    public void setValorBaseFatoGerador(BigDecimal valorBaseFatoGerador) {
        this.valorBaseFatoGerador = valorBaseFatoGerador;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public Long getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(Long codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public Long getNumeroProcessoPexpam() {
        return numeroProcessoPexpam;
    }

    public void setNumeroProcessoPexpam(Long numeroProcessoPexpam) {
        this.numeroProcessoPexpam = numeroProcessoPexpam;
    }

    public Long getAnoProcessoPexpam() {
        return anoProcessoPexpam;
    }

    public void setAnoProcessoPexpam(Long anoProcessoPexpam) {
        this.anoProcessoPexpam = anoProcessoPexpam;
    }

    public BigDecimal getValorTotalMercadoria() {
        return valorTotalMercadoria;
    }

    public void setValorTotalMercadoria(BigDecimal valorTotalMercadoria) {
        this.valorTotalMercadoria = valorTotalMercadoria;
    }

    public BigDecimal getCalculoLimitePLI() {
        return calculoLimitePLI;
    }

    public void setCalculoLimitePLI(BigDecimal calculoLimitePLI) {
        this.calculoLimitePLI = calculoLimitePLI;
    }

    public BigDecimal getValorPreviPLI() {
        return valorPreviPLI;
    }

    public void setValorPreviPLI(BigDecimal valorPreviPLI) {
        this.valorPreviPLI = valorPreviPLI;
    }

    public BigDecimal getValorPercentualReducaoPLI() {
        return valorPercentualReducaoPLI;
    }

    public void setValorPercentualReducaoPLI(BigDecimal valorPercentualReducaoPLI) {
        this.valorPercentualReducaoPLI = valorPercentualReducaoPLI;
    }

    public BigDecimal getValorReduzidoPLI() {
        return valorReduzidoPLI;
    }

    public void setValorReduzidoPLI(BigDecimal valorReduzidoPLI) {
        this.valorReduzidoPLI = valorReduzidoPLI;
    }

    public BigDecimal getValorTCIF() {
        return valorTCIF;
    }

    public void setValorTCIF(BigDecimal valorTCIF) {
        this.valorTCIF = valorTCIF;
    }

    public BigDecimal getValorReduzidoBasePLI() {
        return valorReduzidoBasePLI;
    }

    public void setValorReduzidoBasePLI(BigDecimal valorReduzidoBasePLI) {
        this.valorReduzidoBasePLI = valorReduzidoBasePLI;
    }

    public BigDecimal getValorItensTCIF() {
        return valorItensTCIF;
    }

    public void setValorItensTCIF(BigDecimal valorItensTCIF) {
        this.valorItensTCIF = valorItensTCIF;
    }

    public BigDecimal getValorTotalReduzidoItens() {
        return valorTotalReduzidoItens;
    }

    public void setValorTotalReduzidoItens(BigDecimal valorTotalReduzidoItens) {
        this.valorTotalReduzidoItens = valorTotalReduzidoItens;
    }

    public BigDecimal getValorTotalReduzidoBaseItens() {
        return valorTotalReduzidoBaseItens;
    }

    public void setValorTotalReduzidoBaseItens(BigDecimal valorTotalReduzidoBaseItens) {
        this.valorTotalReduzidoBaseItens = valorTotalReduzidoBaseItens;
    }

    public BigDecimal getValorGeralTCIF() {
        return valorGeralTCIF;
    }

    public void setValorGeralTCIF(BigDecimal valorGeralTCIF) {
        this.valorGeralTCIF = valorGeralTCIF;
    }

    public BigDecimal getValorGeralReducaoTCIF() {
        return valorGeralReducaoTCIF;
    }

    public void setValorGeralReducaoTCIF(BigDecimal valorGeralReducaoTCIF) {
        this.valorGeralReducaoTCIF = valorGeralReducaoTCIF;
    }

    public BigDecimal getValorGeralReduzidoBase() {
        return valorGeralReduzidoBase;
    }

    public void setValorGeralReduzidoBase(BigDecimal valorGeralReduzidoBase) {
        this.valorGeralReduzidoBase = valorGeralReduzidoBase;
    }

    public Long getControleCobrancaTcif() {
        return controleCobrancaTcif;
    }

    public void setControleCobrancaTcif(Long controleCobrancaTcif) {
        this.controleCobrancaTcif = controleCobrancaTcif;
    }

    public Long getNumeroDebito() {
        return numeroDebito;
    }

    public void setNumeroDebito(Long numeroDebito) {
        this.numeroDebito = numeroDebito;
    }

    public Long getAnoDebito() {
        return anoDebito;
    }

    public void setAnoDebito(Long anoDebito) {
        this.anoDebito = anoDebito;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Long getStatusDebito() {
        return statusDebito;
    }

    public void setStatusDebito(Long statusDebito) {
        this.statusDebito = statusDebito;
    }

    public String getStatusMsgDebito() {
        return statusMsgDebito;
    }

    public void setStatusMsgDebito(String statusMsgDebito) {
        this.statusMsgDebito = statusMsgDebito;
    }

    public Date getDataHoraCadastro() {
        return dataHoraCadastro;
    }

    public void setDataHoraCadastro(Date dataHoraCadastro) {
        this.dataHoraCadastro = dataHoraCadastro;
    }

    public BigDecimal getPercentualLimitadorPLI() {
        return percentualLimitadorPLI;
    }

    public void setPercentualLimitadorPLI(BigDecimal percentualLimitadorPLI) {
        this.percentualLimitadorPLI = percentualLimitadorPLI;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return protocoloEnvio;
    }
}

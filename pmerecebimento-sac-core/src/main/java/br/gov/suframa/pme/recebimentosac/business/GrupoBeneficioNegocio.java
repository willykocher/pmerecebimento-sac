package br.gov.suframa.pme.recebimentosac.business;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.GrupoBeneficio;

@Local
public interface GrupoBeneficioNegocio {
    GrupoBeneficio consultarGrupoBeneficioPorId(Long id);
}

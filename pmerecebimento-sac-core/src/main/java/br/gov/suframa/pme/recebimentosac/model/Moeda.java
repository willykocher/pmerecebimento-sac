package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "PMERCB_MOEDA")
public class Moeda extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MOE_CD_MOEDA")
    private Long codigoMoeda;

    @Column(name = "MOE_NO_MOEDA")
    private String nomeMoeda;

    @Column(name = "MOE_DH_CADASTRO")
    private Date dataCadastro;

    public Moeda() {
        // TODO Auto-generated constructor stub
    }

    public Long getCodigoMoeda() {
        return codigoMoeda;
    }

    public void setCodigoMoeda(Long codigoMoeda) {
        this.codigoMoeda = codigoMoeda;
    }

    public String getNomeMoeda() {
        return nomeMoeda;
    }

    public void setNomeMoeda(String nomeMoeda) {
        this.nomeMoeda = nomeMoeda;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

}

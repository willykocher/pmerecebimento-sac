package br.gov.suframa.pme.recebimentosac.business;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.InformacaoBasica;
import br.gov.suframa.pme.recebimentosac.vo.EntradaVO;
import br.gov.suframa.pme.recebimentosac.vo.TipoDebitoRetornoVO;

@Local
public interface InformacaoBasicaNegocio {
    
    List<InformacaoBasica> consultarPendenteDebito();

    List<InformacaoBasica> consultarPendenteDebitoPorIdPLI(Long idPLI);

    InformacaoBasica salvar(InformacaoBasica informacaoBasica);

    InformacaoBasica atualizarStatusInfoBasica(InformacaoBasica pli, Long status, TipoDebitoRetornoVO retorno);

    InformacaoBasica atualizarStatusInfoBasica(EntradaVO entrada, Long status);

    InformacaoBasica consultarPorProtocoloEnvio(Long protocoloEnvio);

    Map<Long, Long> verificarStatusPLIParaInfoBasicas(Set<Long> pli, int condicao);
    
    void atualizarStatusInfoBasica(TipoDebitoRetornoVO retorno, InformacaoBasica informacaoBasica);
}

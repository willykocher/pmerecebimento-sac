package br.gov.suframa.pme.recebimentosac.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import br.gov.suframa.poc.vo.BaseVO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoDebitoRetornoVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @JsonProperty("Numero")
    private Long numero;

    @JsonProperty("AnoDebito")
    private Long anoDebito;

    @JsonProperty("DataVencimento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataVencimento;

    @JsonProperty("TipoDebitoRetorno")
    private Long tipoDebitoRetorno;

    @JsonProperty("Mensagem")
    private String mensagem;

    @JsonProperty("MensagemErro")
    private String mensagemErro;

    public TipoDebitoRetornoVO() {
        // TODO Auto-generated constructor stub
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public long getAnoDebito() {
        return anoDebito;
    }

    public void setAnoDebito(Long anoDebito) {
        this.anoDebito = anoDebito;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Long getTipoDebitoRetorno() {
        return tipoDebitoRetorno;
    }

    public void setTipoDebitoRetorno(Long tipoDebitoRetorno) {
        this.tipoDebitoRetorno = tipoDebitoRetorno;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}

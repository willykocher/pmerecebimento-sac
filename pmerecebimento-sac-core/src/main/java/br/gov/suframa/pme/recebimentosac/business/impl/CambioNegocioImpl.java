package br.gov.suframa.pme.recebimentosac.business.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.suframa.pme.recebimentosac.business.CambioNegocio;
import br.gov.suframa.pme.recebimentosac.model.Cambio;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class CambioNegocioImpl extends NegocioBase implements CambioNegocio {

    @EJB
    private PersistenciaFactory<Long, Cambio> persistencia;

    private static final long CODIGO_MOEDA_DOLAR = 220;

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Cambio consultarCambio(Long codigoMoedaNegociada, Date dataValidade) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<Cambio> query = getCriteriaQuery(Cambio.class);
        Root<Cambio> from = query.from(Cambio.class);
        query.where(cb.and(cb.equal(from.get("codigoMoeda"), codigoMoedaNegociada), cb.lessThanOrEqualTo(from.<Date> get("dataValidade"), dataValidade)));

        Cambio cambio = getEntityManager().createQuery(query).getResultList().size() > 0 ? getEntityManager().createQuery(query).getSingleResult() : null;

        return cambio;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Cambio consultarCambioDolarPorDataValidade(Date dataValidade) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<Cambio> query = getCriteriaQuery(Cambio.class);
        Root<Cambio> from = query.from(Cambio.class);
        query.where(cb.and(cb.equal(from.get("codigoMoeda"), CODIGO_MOEDA_DOLAR), cb.lessThanOrEqualTo(from.<Date> get("dataValidade"), dataValidade)));
        // cb.equal(from.get("dataValidade"), dataValidade)
        Cambio cambio = getEntityManager().createQuery(query).getResultList().size() > 0 ? getEntityManager().createQuery(query).getSingleResult() : null;

        return cambio;
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistencia.getEntityManager();
    }

}

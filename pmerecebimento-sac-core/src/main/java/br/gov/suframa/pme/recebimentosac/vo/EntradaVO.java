package br.gov.suframa.pme.recebimentosac.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.gov.suframa.poc.vo.BaseVO;

public class EntradaVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @JsonProperty("numeroSolicitacao")
    private Long numeroSolicitacao;

    @JsonProperty("dataOperacao")
    private String dataOperacao;

    public EntradaVO() {
        // TODO Auto-generated constructor stub
    }

    public Long getNumeroSolicitacao() {
        return numeroSolicitacao;
    }

    public void setNumeroSolicitacao(Long numeroSolicitacao) {
        this.numeroSolicitacao = numeroSolicitacao;
    }

    public String getDataOperacao() {
        return dataOperacao;
    }

    public void setDataOperacao(String dataOperacao) {
        this.dataOperacao = dataOperacao;
    }
}

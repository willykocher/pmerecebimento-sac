package br.gov.suframa.pme.recebimentosac.business;

import java.util.Date;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.Cambio;

@Local
public interface CambioNegocio {
    Cambio consultarCambio(Long codigoMoedaNegociada, Date dataValidade);

    Cambio consultarCambioDolarPorDataValidade(Date dataValidade);
}

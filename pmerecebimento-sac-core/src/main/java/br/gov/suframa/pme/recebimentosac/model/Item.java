package br.gov.suframa.pme.recebimentosac.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "PMERCB_ITEM")
public class Item extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ITE_MER_IBA_NU_INS_SUF")
    private Long inscricaoSuframa;

    @Column(name = "ITE_MER_IBA_NU_PLI")
    private String numeroPLI;

    @Column(name = "ITE_MER_CD_NCM")
    private String codigoNCM;

    @Column(name = "ITE_MER_CD_PRODUTO")
    private String codigoProduto;

    @Column(name = "ITE_MER_CD_TIPO")
    private String codigoTipo;

    @Column(name = "ITE_MER_CD_MODELO")
    private String codigoModelo;

    @Column(name = "ITE_MER_NU_MERCADORIA")
    private Long nuMercadoria;

    @Column(name = "ITE_NU_INSUMO")
    private Long numeroInsumo;

    @Column(name = "ITE_DS_INSUMO")
    private String descricaoInsumo;

    @Column(name = "ITE_VL_TCIF_ITEM")
    private Double valorTCIFItem;

    @Column(name = "ITE_VL_REDUCAO_ITEM")
    private Double valorReducaoItem;

    @Column(name = "ITE_VL_UNID_REAIS")
    private Double valorUnidadeReais;

    @Column(name = "ITE_VL_BASE_FATO_GERADOR_ITEM")
    private Double valorBaseFatoGeradorItem;

    @Column(name = "ITE_VL_PERC_LIMITADOR_ITEM")
    private Double valorPercentualLimitadorItem;

    @Column(name = "ITE_VL_PREVALENCIA_ITEM")
    private Double valorPrevalenciaItem;

    public Item() {
        // TODO Auto-generated constructor stub
    }

    public Long getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Long inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    public String getCodigoNCM() {
        return codigoNCM;
    }

    public void setCodigoNCM(String codigoNCM) {
        this.codigoNCM = codigoNCM;
    }

    public Long getNumeroInsumo() {
        return numeroInsumo;
    }

    public void setNumeroInsumo(Long numeroInsumo) {
        this.numeroInsumo = numeroInsumo;
    }

    public String getDescricaoInsumo() {
        return descricaoInsumo;
    }

    public void setDescricaoInsumo(String descricaoInsumo) {
        this.descricaoInsumo = descricaoInsumo;
    }

    public Double getValorTCIFItem() {
        return valorTCIFItem;
    }

    public void setValorTCIFItem(Double valorTCIFItem) {
        this.valorTCIFItem = valorTCIFItem;
    }

    public Double getValorReducaoItem() {
        return valorReducaoItem;
    }

    public void setValorReducaoItem(Double valorReducaoItem) {
        this.valorReducaoItem = valorReducaoItem;
    }

    public Double getValorUnidadeReais() {
        return valorUnidadeReais;
    }

    public void setValorUnidadeReais(Double valorUnidadeReais) {
        this.valorUnidadeReais = valorUnidadeReais;
    }

    public Double getValorBaseFatoGeradorItem() {
        return valorBaseFatoGeradorItem;
    }

    public void setValorBaseFatoGeradorItem(Double valorBaseFatoGeradorItem) {
        this.valorBaseFatoGeradorItem = valorBaseFatoGeradorItem;
    }

    public Double getValorPercentualLimitadorItem() {
        return valorPercentualLimitadorItem;
    }

    public void setValorPercentualLimitadorItem(Double valorPercentualLimitadorItem) {
        this.valorPercentualLimitadorItem = valorPercentualLimitadorItem;
    }

    public Double getValorPrevalenciaItem() {
        return valorPrevalenciaItem;
    }

    public void setValorPrevalenciaItem(Double valorPrevalenciaItem) {
        this.valorPrevalenciaItem = valorPrevalenciaItem;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getCodigoTipo() {
        return codigoTipo;
    }

    public void setCodigoTipo(String codigoTipo) {
        this.codigoTipo = codigoTipo;
    }

    public String getCodigoModelo() {
        return codigoModelo;
    }

    public void setCodigoModelo(String codigoModelo) {
        this.codigoModelo = codigoModelo;
    }

    public Long getNuMercadoria() {
        return nuMercadoria;
    }

    public void setNuMercadoria(Long nuMercadoria) {
        this.nuMercadoria = nuMercadoria;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

}

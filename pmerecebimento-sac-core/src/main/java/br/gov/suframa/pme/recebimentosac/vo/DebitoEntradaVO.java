package br.gov.suframa.pme.recebimentosac.vo;

import java.math.BigDecimal;
import java.util.Date;

import br.gov.suframa.pme.recebimentosac.model.InformacaoBasica;
import br.gov.suframa.pme.recebimentosac.util.DateUtil;
import br.gov.suframa.poc.vo.BaseVO;

public class DebitoEntradaVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private ServicoDebitoVO servico;
    private Long numeroProtocoloEnvio;
    private Integer anoCorrente;
    private String numeroPLI;
    private Long inscricaoSuframa;
    private String cnpjCpfImportador;
    private String razaoSocialImportador;
    private Long cobrarDebito;
    private Integer localidade;
    private BigDecimal valorDebito;
    private BigDecimal valorTcifPli;
    private BigDecimal valorTcifItens;
    private BigDecimal valorReducao;
    private ExtratoDebitoVO extratoEstrangeiro;

    public DebitoEntradaVO() {
        // TODO Auto-generated constructor stub
    }

    public DebitoEntradaVO(InformacaoBasica info, ExtratoDebitoVO extrato) {
        this.servico = new ServicoDebitoVO(info.getCodigoServico());
        this.numeroProtocoloEnvio = info.getProtocoloEnvio();
        this.anoCorrente = DateUtil.getYear(new Date());
        this.numeroPLI = info.getNumeroPLI();
        this.inscricaoSuframa = info.getInscricaoSuframa();
        this.cnpjCpfImportador = info.getCnpjCpf() != null ? info.getCnpjCpf().toString() : null;
        this.razaoSocialImportador = info.getNomeEmpresa();
        this.cobrarDebito = info.getControleCobrancaTcif();
        this.localidade = Integer.valueOf(info.getLocalidade());
        this.valorDebito = info.getValorGeralTCIF();
        this.valorTcifPli = info.getValorTCIF();
        this.valorTcifItens = info.getValorItensTCIF();
        this.valorReducao = info.getValorGeralReducaoTCIF();
        this.extratoEstrangeiro = extrato;
    }

    public ServicoDebitoVO getServico() {
        return servico;
    }

    public void setServico(ServicoDebitoVO servico) {
        this.servico = servico;
    }

    public Long getNumeroProtocoloEnvio() {
        return numeroProtocoloEnvio;
    }

    public void setNumeroProtocoloEnvio(Long numeroProtocoloEnvio) {
        this.numeroProtocoloEnvio = numeroProtocoloEnvio;
    }

    public Integer getAnoCorrente() {
        return anoCorrente;
    }

    public void setAnoCorrente(Integer anoCorrente) {
        this.anoCorrente = anoCorrente;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    public Long getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Long inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getCnpjCpfImportador() {
        return cnpjCpfImportador;
    }

    public void setCnpjCpfImportador(String cnpjCpfImportador) {
        this.cnpjCpfImportador = cnpjCpfImportador;
    }

    public String getRazaoSocialImportador() {
        return razaoSocialImportador;
    }

    public void setRazaoSocialImportador(String razaoSocialImportador) {
        this.razaoSocialImportador = razaoSocialImportador;
    }

    public Long getCobrarDebito() {
        return cobrarDebito;
    }

    public void setCobrarDebito(Long cobrarDebito) {
        this.cobrarDebito = cobrarDebito;
    }

    public Integer getLocalidade() {
        return localidade;
    }

    public void setLocalidade(Integer localidade) {
        this.localidade = localidade;
    }

    public BigDecimal getValorDebito() {
        return valorDebito;
    }

    public void setValorDebito(BigDecimal valorDebito) {
        this.valorDebito = valorDebito;
    }

    public BigDecimal getValorTcifPli() {
        return valorTcifPli;
    }

    public void setValorTcifPli(BigDecimal valorTcifPli) {
        this.valorTcifPli = valorTcifPli;
    }

    public BigDecimal getValorTcifItens() {
        return valorTcifItens;
    }

    public void setValorTcifItens(BigDecimal valorTcifItens) {
        this.valorTcifItens = valorTcifItens;
    }

    public BigDecimal getValorReducao() {
        return valorReducao;
    }

    public void setValorReducao(BigDecimal valorReducao) {
        this.valorReducao = valorReducao;
    }

    public ExtratoDebitoVO getExtratoEstrangeiro() {
        return extratoEstrangeiro;
    }

    public void setExtratoEstrangeiro(ExtratoDebitoVO extratoEstrangeiro) {
        this.extratoEstrangeiro = extratoEstrangeiro;
    }
}

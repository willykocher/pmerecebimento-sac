package br.gov.suframa.pme.recebimentosac.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.suframa.pme.recebimentosac.business.ItemNegocio;
import br.gov.suframa.pme.recebimentosac.model.Item;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class ItemNegocioImpl extends NegocioBase implements ItemNegocio {

    @EJB
    private PersistenciaFactory<Long, Item> persistencia;

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Item> consultarItemDeMercadoria(Long inscricaoSuframa, String numeroPLI, String codigoNCM, String codigoProduto, String codigoTipo, String codigoModelo,
            Long nuMercadoria) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<Item> query = getCriteriaQuery(Item.class);
        Root<Item> from = query.from(Item.class);
        query.where(cb.and(cb.equal(from.get("inscricaoSuframa"), inscricaoSuframa)),

        cb.equal(from.get("numeroPLI"), numeroPLI), cb.equal(from.get("codigoNCM"), codigoNCM), cb.equal(from.get("codigoProduto"), codigoProduto),
                cb.equal(from.get("codigoTipo"), codigoTipo), cb.equal(from.get("codigoModelo"), codigoModelo), cb.equal(from.get("nuMercadoria"), nuMercadoria));

        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistencia.getEntityManager();
    }
}

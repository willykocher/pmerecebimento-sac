package br.gov.suframa.pme.recebimentosac.business;

import javax.ejb.Local;

@Local
public interface AutenticacaoNegocio {

    boolean autenticar(String usuario, String senha);

}

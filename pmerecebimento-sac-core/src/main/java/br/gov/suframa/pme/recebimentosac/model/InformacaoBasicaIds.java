package br.gov.suframa.pme.recebimentosac.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class InformacaoBasicaIds implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 4608076006537552175L;

    private Long inscricaoSuframa;

    private String numeroPLI;

    public Long getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Long inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    @Override
    public int hashCode() {
        return inscricaoSuframa.hashCode() + numeroPLI.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if ((o instanceof InformacaoBasicaIds) && ((InformacaoBasicaIds) o).getNumeroPLI().equals(this.getNumeroPLI())
                && ((InformacaoBasicaIds) o).getInscricaoSuframa() == this.getInscricaoSuframa()) {
            return true;
        } else
            return false;
    }

}

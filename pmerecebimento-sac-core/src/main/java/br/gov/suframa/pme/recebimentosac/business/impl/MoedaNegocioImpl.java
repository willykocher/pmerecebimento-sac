package br.gov.suframa.pme.recebimentosac.business.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.suframa.pme.recebimentosac.business.MoedaNegocio;
import br.gov.suframa.pme.recebimentosac.model.Moeda;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class MoedaNegocioImpl extends NegocioBase implements MoedaNegocio {

    @EJB
    private PersistenciaFactory<Long, Moeda> persistencia;

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Moeda consultarMoeda(Long codigoMoeda) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<Moeda> query = getCriteriaQuery(Moeda.class);
        Root<Moeda> from = query.from(Moeda.class);
        query.where(cb.equal(from.get("codigoMoeda"), codigoMoeda));

        Moeda moeda = getEntityManager().createQuery(query).getResultList().size() > 0 ? getEntityManager().createQuery(query).getSingleResult() : null;

        return moeda;
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistencia.getEntityManager();
    }

}

package br.gov.suframa.pme.recebimentosac.persistence.impl;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.gov.suframa.poc.model.EntidadeBase;
import br.gov.suframa.poc.persistence.impl.PersistenceBase;

@Stateless
public class PersistenciaFactory<PK extends Serializable, T extends EntidadeBase<PK>> extends PersistenceBase<PK, T> {

    @PersistenceContext(unitName = "suframa-em")
    private EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Query executeNativeQuery(String sql) {
        return this.getEntityManager().createNativeQuery(sql);
    }

}

package br.gov.suframa.pme.recebimentosac.business.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.gov.suframa.pme.recebimentosac.business.AutenticacaoNegocio;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class AutenticacaoNegocioImpl extends NegocioBase implements AutenticacaoNegocio {

    @Inject
    private Logger logger;

    @EJB
    private PersistenciaFactory<?, ?> persistencia;

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    @Transactional
    @Override
    public boolean autenticar(String usuario, String senha) {
        String sql = "SELECT USU_LOGIN FROM PSS.SAA_USUARIO@PSSPUB WHERE USU_LOGIN = ? AND USU_SENHA = ?";
        Query query = persistencia.executeNativeQuery(sql);
        query.setParameter(1, usuario);
        query.setParameter(2, senha);
        String usuarioRecuperado;
        try {
            usuarioRecuperado = (String) query.getSingleResult();
        } catch (NoResultException e) {
            logger.log(Level.SEVERE, "Nenhum usuario encontrado", e);
            return false;
        }
        if (usuarioRecuperado.contentEquals(usuario)) {
            return true;
        }
        return false;
    }
}

package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "PMERCB_MERCADORIA")
public class Mercadoria extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MER_IBA_NU_INS_SUF")
    private Long inscricaoSuframa;

    @Column(name = "MER_IBA_NU_PLI")
    private String numeroPLI;

    @Column(name = "MER_CAM_DT_VALIDADE")
    private Date cambioDataValidade;

    @Column(name = "MER_CD_NCM")
    private String codigoNCM;

    @Column(name = "MER_CAM_CD_MOEDA_NEGOCIADA")
    private Long codigoMoedaNegociada;

    @Column(name = "MER_VL_MERC_MOEDA_NEGOCIADA")
    private Float valorMercadoriaMoedaNegociada;

    @Column(name = "MER_VL_MERC_REAIS")
    private Float valorMercadoriaReais;

    @Column(name = "MER_QT_ITENS")
    private Long quatidadeItens;

    @Column(name = "MER_GBE_ID_ISENCAO")
    private Long grupoBeneficio;

    @Column(name = "MER_CD_PRODUTO")
    private String codigoProduto;

    @Column(name = "MER_CD_TIPO")
    private String codigoTipo;

    @Column(name = "MER_CD_MODELO")
    private String codigoModelo;

    @Column(name = "MER_NU_MERCADORIA")
    private Long nuMercadoria;

    public Mercadoria() {
        // TODO Auto-generated constructor stub
    }

    public Long getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Long inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    public Date getCambioDataValidade() {
        return cambioDataValidade;
    }

    public void setCambioDataValidade(Date cambioDataValidade) {
        this.cambioDataValidade = cambioDataValidade;
    }

    public String getCodigoNCM() {
        return codigoNCM;
    }

    public void setCodigoNCM(String codigoNCM) {
        this.codigoNCM = codigoNCM;
    }

    public Float getValorMercadoriaMoedaNegociada() {
        return valorMercadoriaMoedaNegociada;
    }

    public void setValorMercadoriaMoedaNegociada(Float valorMercadoriaMoedaNegociada) {
        this.valorMercadoriaMoedaNegociada = valorMercadoriaMoedaNegociada;
    }

    public Long getQuatidadeItens() {
        return quatidadeItens;
    }

    public void setQuatidadeItens(Long quatidadeItens) {
        this.quatidadeItens = quatidadeItens;
    }

    public Long getCodigoMoedaNegociada() {
        return codigoMoedaNegociada;
    }

    public void setCodigoMoedaNegociada(Long codigoMoedaNegociada) {
        this.codigoMoedaNegociada = codigoMoedaNegociada;
    }

    public Float getValorMercadoriaReais() {
        return valorMercadoriaReais;
    }

    public void setValorMercadoriaReais(Float valorMercadoriaReais) {
        this.valorMercadoriaReais = valorMercadoriaReais;
    }

    public Long getGrupoBeneficio() {
        return grupoBeneficio;
    }

    public void setGrupoBeneficio(Long grupoBeneficio) {
        this.grupoBeneficio = grupoBeneficio;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getCodigoTipo() {
        return codigoTipo;
    }

    public void setCodigoTipo(String codigoTipo) {
        this.codigoTipo = codigoTipo;
    }

    public String getCodigoModelo() {
        return codigoModelo;
    }

    public void setCodigoModelo(String codigoModelo) {
        this.codigoModelo = codigoModelo;
    }

    public Long getNuMercadoria() {
        return nuMercadoria;
    }

    public void setNuMercadoria(Long nuMercadoria) {
        this.nuMercadoria = nuMercadoria;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

}

package br.gov.suframa.pme.recebimentosac.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.suframa.pme.recebimentosac.business.MercadoriaNegocio;
import br.gov.suframa.pme.recebimentosac.model.Mercadoria;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class MercadoriaNegocioImpl extends NegocioBase implements MercadoriaNegocio {

    @EJB
    private PersistenciaFactory<Long, Mercadoria> persistencia;

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Mercadoria> consultarMercadorias(Long inscricaoSuframa, String numeroPLI) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<Mercadoria> query = getCriteriaQuery(Mercadoria.class);
        Root<Mercadoria> from = query.from(Mercadoria.class);
        query.where(cb.and(cb.equal(from.get("inscricaoSuframa"), inscricaoSuframa)), cb.equal(from.get("numeroPLI"), numeroPLI));

        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistencia.getEntityManager();
    }
}

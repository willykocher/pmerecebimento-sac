package br.gov.suframa.pme.recebimentosac.vo;

import br.gov.suframa.pme.recebimentosac.model.Item;
import br.gov.suframa.poc.vo.BaseVO;

public class ItemVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Long numero;
    private String nomeItem;
    private String valorItemEmReal;
    private String valorTCIFBaseItem;
    private String limitadorItem;
    private String valorTCIFRealItem;
    private String isencao;
    private Double reducao;

    public ItemVO() {
        // TODO Auto-generated constructor stub
    }

    public ItemVO(Item item, String isencao) {
        this.numero = item.getNumeroInsumo();
        this.nomeItem = item.getNumeroInsumo() != null ? item.getNumeroInsumo().toString() : null;
        this.valorItemEmReal = item.getValorUnidadeReais() != null ? item.getValorUnidadeReais().toString() : null;
        this.valorTCIFBaseItem = item.getValorBaseFatoGeradorItem() != null ? item.getValorBaseFatoGeradorItem().toString() : null;
        this.limitadorItem = item.getValorPercentualLimitadorItem() != null ? item.getValorPercentualLimitadorItem().toString() : null;
        this.valorTCIFRealItem = item.getValorPrevalenciaItem() != null ? item.getValorPrevalenciaItem().toString() : null;
        this.isencao = isencao;
        this.reducao = item.getValorReducaoItem();
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getNomeItem() {
        return nomeItem;
    }

    public void setNomeItem(String nomeItem) {
        this.nomeItem = nomeItem;
    }

    public String getValorItemEmReal() {
        return valorItemEmReal;
    }

    public void setValorItemEmReal(String valorItemEmReal) {
        this.valorItemEmReal = valorItemEmReal;
    }

    public String getValorTCIFBaseItem() {
        return valorTCIFBaseItem;
    }

    public void setValorTCIFBaseItem(String valorTCIFBaseItem) {
        this.valorTCIFBaseItem = valorTCIFBaseItem;
    }

    public String getLimitadorItem() {
        return limitadorItem;
    }

    public void setLimitadorItem(String limitadorItem) {
        this.limitadorItem = limitadorItem;
    }

    public String getValorTCIFRealItem() {
        return valorTCIFRealItem;
    }

    public void setValorTCIFRealItem(String valorTCIFRealItem) {
        this.valorTCIFRealItem = valorTCIFRealItem;
    }

    public String getIsencao() {
        return isencao;
    }

    public void setIsencao(String isencao) {
        this.isencao = isencao;
    }

    public Double getReducao() {
        return reducao;
    }

    public void setReducao(Double reducao) {
        this.reducao = reducao;
    }
}

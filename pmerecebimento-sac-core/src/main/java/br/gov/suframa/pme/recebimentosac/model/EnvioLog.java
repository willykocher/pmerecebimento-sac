package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "PMERCB_ENVIO_LOG")
public class EnvioLog extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 3480797408942910402L;

    @Id
    @Column(name = "ELO_IBA_NU_INS_SUF")
    private Long inscricaoSuframa;

    @Column(name = "ELO_IBA_NU_PLI")
    private String numeroPLI;

    @Column(name = "ELO_IBA_PLI_ID")
    private Long protocoloEnvio;

    @Column(name = "ELO_TLO_CD_TP_LOG")
    private Integer codTipoLog;

    @Column(name = "ELO_MS_LOG")
    private String msgLog;

    @Column(name = "ELO_DH_CADASTRO")
    private Date dtCadastro;

    public Long getInscricaoSuframa() {
        return inscricaoSuframa;
    }

    public void setInscricaoSuframa(Long inscricaoSuframa) {
        this.inscricaoSuframa = inscricaoSuframa;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    public Long getProtocoloEnvio() {
        return protocoloEnvio;
    }

    public void setProtocoloEnvio(Long protocoloEnvio) {
        this.protocoloEnvio = protocoloEnvio;
    }

    public Integer getCodTipoLog() {
        return codTipoLog;
    }

    public void setCodTipoLog(Integer codTipoLog) {
        this.codTipoLog = codTipoLog;
    }

    public String getMsgLog() {
        return msgLog;
    }

    public void setMsgLog(String msgLog) {
        this.msgLog = msgLog;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

}

package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "TC_TIPO_DOCUMENTO")
public class TipoDocumento extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public TipoDocumento() {
        // TODO Auto-generated constructor stub
    }

    @Id
    @Column(name = "TDO_CD")
    private String codigo;

    @Column(name = "TDO_SER_CD")
    private Long codigoServico;

    @Column(name = "TDO_DS")
    private String descricao;

    @Column(name = "TDO_DH_CADASTRO")
    private Date dataCadastro;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Long getCodigoServico() {
        return codigoServico;
    }

    public void setCodigoServico(Long codigoServico) {
        this.codigoServico = codigoServico;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

}

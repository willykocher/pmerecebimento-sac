package br.gov.suframa.pme.recebimentosac.vo;

import br.gov.suframa.poc.vo.BaseVO;

public class GerarDebitoManualVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private String jsonEnvio;
    private String jsonResposta;

    public GerarDebitoManualVO() {
        // TODO Auto-generated constructor stub
    }

    public GerarDebitoManualVO(String jsonEnvio, String jsonResposta) {
        this.jsonEnvio = jsonEnvio;
        this.jsonResposta = jsonResposta;
    }

    public String getJsonEnvio() {
        return jsonEnvio;
    }

    public void setJsonEnvio(String jsonEnvio) {
        this.jsonEnvio = jsonEnvio;
    }

    public String getJsonResposta() {
        return jsonResposta;
    }

    public void setJsonResposta(String jsonResposta) {
        this.jsonResposta = jsonResposta;
    }
}

package br.gov.suframa.pme.recebimentosac.vo;

public class ProtocoloEnvioStatus {

    private Long protocolo;

    private Long status;

    public Long getProtocolo() {
        return protocolo;
    }

    public Long getStatus() {
        return status;
    }

    public void setProtocolo(Long protocolo) {
        this.protocolo = protocolo;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ProtocoloEnvioStatus() {
    }

}

package br.gov.suframa.pme.recebimentosac.business;

import java.util.List;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.Mercadoria;

@Local
public interface MercadoriaNegocio {

    List<Mercadoria> consultarMercadorias(Long inscricaoSuframa, String numeroPLI);
}

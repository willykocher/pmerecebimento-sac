package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "PMERCB_PEDIDOLICIMPORT")
public class PedidoLicenciamentoImportacao extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public static final int LIQUIDACAO = 1;
    public static final int CANCELAMENTO = 2;

    public static final int LIBERA_OU_AGUARDA = 0;

    @Id
    @Column(name = "PLI_ID")
    private Long pliId;

    @Column(name = "VERSION_ID")
    private Long versionId;

    @Column(name = "PLI_DHENVIO")
    private Date dataEnvio;

    @Column(name = "PLI_PROCESSADO")
    private Long processado;

    @Lob
    @Column(name = "PLI_CONTEUDO_ARQUIVO")
    private String conteudoArquivo;

    @Column(name = "PLI_QUANTIDADE")
    private Long quantidade;

    @Column(name = "PLI_ESTRUT_SUF")
    private Long estruturaSuframa;

    @Column(name = "PLI_USI_ID")
    private String usuarioId;

    @Column(name = "PLI_NOME_ARQUIVO")
    private String nomeArquivo;

    @Column(name = "PLI_TIPO_PROCESSAMENTO")
    private Long tipoProcessado;

    public PedidoLicenciamentoImportacao() {

    }

    public Long getPliId() {
        return pliId;
    }

    public void setPliId(Long pliId) {
        this.pliId = pliId;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public Long getProcessado() {
        return processado;
    }

    public void setProcessado(Long processado) {
        this.processado = processado;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }

    public Long getEstruturaSuframa() {
        return estruturaSuframa;
    }

    public void setEstruturaSuframa(Long estruturaSuframa) {
        this.estruturaSuframa = estruturaSuframa;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public Long getTipoProcessado() {
        return tipoProcessado;
    }

    public void setTipoProcessado(Long tipoProcessado) {
        this.tipoProcessado = tipoProcessado;
    }

    public String getConteudoArquivo() {
        return conteudoArquivo;
    }

    public void setConteudoArquivo(String conteudoArquivo) {
        this.conteudoArquivo = conteudoArquivo;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return pliId;
    }

}

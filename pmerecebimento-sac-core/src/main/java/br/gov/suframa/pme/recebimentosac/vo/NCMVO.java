package br.gov.suframa.pme.recebimentosac.vo;

import java.util.Date;
import java.util.List;

import br.gov.suframa.pme.recebimentosac.model.Mercadoria;
import br.gov.suframa.poc.vo.BaseVO;

public class NCMVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private Long numeroNCM;
    private Float valorMercadoriaMoedaNegociada;
    private Float valorMercadoriaEmReal;
    private String moedaCambio;
    private Date dataCambio;
    private Float taxaCambio;
    private Integer qtdItens;
    private List<ItemVO> itens;

    public NCMVO() {
        // TODO Auto-generated constructor stub
    }

    public NCMVO(Mercadoria mercadoria, String moedaCambio, Float taxaCambio, Integer qtdItens, List<ItemVO> itens) {
        this.numeroNCM = Long.valueOf(mercadoria.getCodigoNCM());
        this.valorMercadoriaMoedaNegociada = mercadoria.getValorMercadoriaMoedaNegociada();
        this.valorMercadoriaEmReal = mercadoria.getValorMercadoriaReais();
        this.moedaCambio = moedaCambio;
        this.dataCambio = mercadoria.getCambioDataValidade();
        this.taxaCambio = taxaCambio;
        this.qtdItens = qtdItens;
        this.itens = itens;
    }

    public Long getNumeroNCM() {
        return numeroNCM;
    }

    public void setNumeroNCM(Long numeroNCM) {
        this.numeroNCM = numeroNCM;
    }

    public Float getValorMercadoriaMoedaNegociada() {
        return valorMercadoriaMoedaNegociada;
    }

    public void setValorMercadoriaMoedaNegociada(Float valorMercadoriaMoedaNegociada) {
        this.valorMercadoriaMoedaNegociada = valorMercadoriaMoedaNegociada;
    }

    public Float getValorMercadoriaEmReal() {
        return valorMercadoriaEmReal;
    }

    public void setValorMercadoriaEmReal(Float valorMercadoriaEmReal) {
        this.valorMercadoriaEmReal = valorMercadoriaEmReal;
    }

    public String getMoedaCambio() {
        return moedaCambio;
    }

    public void setMoedaCambio(String moedaCambio) {
        this.moedaCambio = moedaCambio;
    }

    public Date getDataCambio() {
        return dataCambio;
    }

    public void setDataCambio(Date dataCambio) {
        this.dataCambio = dataCambio;
    }

    public Float getTaxaCambio() {
        return taxaCambio;
    }

    public void setTaxaCambio(Float taxaCambio) {
        this.taxaCambio = taxaCambio;
    }

    public Integer getQtdItens() {
        return qtdItens;
    }

    public void setQtdItens(Integer qtdItens) {
        this.qtdItens = qtdItens;
    }

    public List<ItemVO> getItens() {
        return itens;
    }

    public void setItens(List<ItemVO> itens) {
        this.itens = itens;
    }
}

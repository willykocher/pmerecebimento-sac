package br.gov.suframa.pme.recebimentosac.vo;

import br.gov.suframa.poc.vo.BaseVO;

public class MessageVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private Integer status;
    private String dataHora;
    private String descricao;

    public MessageVO() {
        // TODO Auto-generated constructor stub
    }

    public MessageVO(String descricao) {
        this.descricao = descricao;
    }

    public MessageVO(Integer status, String dataHora, String descricao) {
        this.status = status;
        this.dataHora = dataHora;
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }
}

package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "PMERCB_CAMBIO")
public class Cambio extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CAM_CD_MOEDA")
    private Long codigoMoeda;

    @Column(name = "CAM_VL_FATOR")
    private Float valorFator;

    @Column(name = "CAM_DT_VALIDADE")
    private Date dataValidade;

    public Cambio() {
        // TODO Auto-generated constructor stub
    }

    public Long getCodigoMoeda() {
        return codigoMoeda;
    }

    public void setCodigoMoeda(Long codigoMoeda) {
        this.codigoMoeda = codigoMoeda;
    }

    public Float getValorFator() {
        return valorFator;
    }

    public void setValorFator(Float valorFator) {
        this.valorFator = valorFator;
    }

    public Date getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(Date dataValidade) {
        this.dataValidade = dataValidade;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }
}

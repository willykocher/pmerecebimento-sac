package br.gov.suframa.pme.recebimentosac.business.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.gov.suframa.pme.recebimentosac.business.EnvioLogNegocio;
import br.gov.suframa.pme.recebimentosac.business.InformacaoBasicaNegocio;
import br.gov.suframa.pme.recebimentosac.model.EnvioLog;
import br.gov.suframa.pme.recebimentosac.model.InformacaoBasica;
import br.gov.suframa.pme.recebimentosac.model.PedidoLicenciamentoImportacao;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.pme.recebimentosac.vo.EntradaVO;
import br.gov.suframa.pme.recebimentosac.vo.TipoDebitoRetornoVO;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class InformacaoBasicaNegocioImpl extends NegocioBase implements InformacaoBasicaNegocio {

    @EJB
    private PersistenciaFactory<Long, InformacaoBasica> persistenciaInfoBasica;

    @Inject
    private Logger logger;

    private static final long STATUS_PENDENTE = 1;
    private static final long STATUS_DEBITO_EXISTENTE = 2L;
    private static final long STATUS_ERRO_ENVIO = 3;
    private static final long STATUS_DEBITO_GERADO_PAGO = 4;
    private static final long STATUS_DEBITO_GERADO_CANCELADO = 5;
    private static final long STATUS_DEBITO_ERRO = 6;

    private static final long STATUS_DEBITO_GERADO = 2L;
    private static final long TIPO_DEBITO_AUTORIZADO = 3L;
    private static final long TIPO_DEBITO_RETORNO_ERRO = 0L;
    private static final long TIPO_DEBITO_RETORNO_EXISTENTE = 2L;
    private static final long TIPO_DEBITO_RETORNO_SUCESSO = 1L;

    private static final int LOG_ERRO_ESTRUTURA = 1;
    private static final int LOG_DEBITO_EXISTENTE = 3;

    @EJB
    private EnvioLogNegocio envioLogNegocio;

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<InformacaoBasica> consultarPendenteDebito() {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<InformacaoBasica> query = getCriteriaQuery(InformacaoBasica.class);
        Root<InformacaoBasica> from = query.from(InformacaoBasica.class);

        List<Order> listOrder = new ArrayList<>();

        listOrder.add(criteriaBuilder.desc(from.get("dataHoraCadastro")));

        query.where(from.get("statusDebito").in(STATUS_PENDENTE, STATUS_ERRO_ENVIO));
        query.orderBy(listOrder);

        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<InformacaoBasica> consultarPendenteDebitoPorIdPLI(Long idPLI) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<InformacaoBasica> query = getCriteriaQuery(InformacaoBasica.class);
        Root<InformacaoBasica> from = query.from(InformacaoBasica.class);
        query.where(cb.and(from.get("statusDebito").in(STATUS_PENDENTE, STATUS_ERRO_ENVIO), cb.equal(from.get("protocoloEnvio"), idPLI)));

        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    public InformacaoBasica salvar(InformacaoBasica informacaoBasica) {
        InformacaoBasica salvo = persistenciaInfoBasica.gravar(informacaoBasica);
        return salvo;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public InformacaoBasica consultarPorProtocoloEnvio(Long protocoloEnvio) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<InformacaoBasica> query = getCriteriaQuery(InformacaoBasica.class);
        Root<InformacaoBasica> from = query.from(InformacaoBasica.class);
        query.where(cb.equal(from.get("protocoloEnvio"), protocoloEnvio));

        return !getEntityManager().createQuery(query).getResultList().isEmpty() ? getEntityManager().createQuery(query).getSingleResult() : null;
    }

    @Override
    @Transactional
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public InformacaoBasica atualizarStatusInfoBasica(InformacaoBasica infoBasicas, Long status, TipoDebitoRetornoVO retorno) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<InformacaoBasica> query = getCriteriaQuery(InformacaoBasica.class);
        Root<InformacaoBasica> from = query.from(InformacaoBasica.class);
        query.where(cb.and(cb.equal(from.get("inscricaoSuframa"), infoBasicas.getInscricaoSuframa())), cb.equal(from.get("numeroPLI"), infoBasicas.getNumeroPLI()));

        InformacaoBasica info = !getEntityManager().createQuery(query).getResultList().isEmpty() ? getEntityManager().createQuery(query).getSingleResult() : null;

        if (info != null && retorno == null) {
            info.setStatusDebito(status);
            logger.log(Level.INFO, "NumPLI: " + info.getNumeroPLI());
            return persistenciaInfoBasica.gravar(info);
        }

        return info;
    }

    @Override
    @Transactional
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public InformacaoBasica atualizarStatusInfoBasica(EntradaVO entrada, Long status) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<InformacaoBasica> query = getCriteriaQuery(InformacaoBasica.class);
        Root<InformacaoBasica> from = query.from(InformacaoBasica.class);
        query.where(cb.equal(from.get("protocoloEnvio"), entrada.getNumeroSolicitacao()));

        List<InformacaoBasica> listInfo = getEntityManager().createQuery(query).getResultList();

        for (InformacaoBasica info : listInfo) {
            if (!entrada.getDataOperacao().trim().isEmpty()) {
                SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    if (status == STATUS_DEBITO_GERADO_PAGO) {
                        info.setDataDebitoPagamento(sdfDate.parse(entrada.getDataOperacao()));
                    } else if (status == STATUS_DEBITO_GERADO_CANCELADO) {
                        info.setDataDebitoCancelamento(sdfDate.parse(entrada.getDataOperacao()));
                    }
                } catch (ParseException e) {
                    logger.log(Level.SEVERE, "ERRO", e);
                }
            }
            if (info.getStatusDebito() == 2 || info.getStatusDebito() == 9) {
                info.setStatusDebito(status);
            }
            persistenciaInfoBasica.gravar(info);
        }

        return null;
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistenciaInfoBasica.getEntityManager();
    }

    @Override
    public Map<Long, Long> verificarStatusPLIParaInfoBasicas(Set<Long> pli, int condicao) {
        if (pli.isEmpty()) {
            return new HashMap<>();
        }

        String ids = StringUtils.join(pli, ",");
        Query query = persistenciaInfoBasica.getEntityManager().createQuery(
                "SELECT distinct ib.protocoloEnvio, ib.statusDebito FROM InformacaoBasica ib where ib.protocoloEnvio in (" + ids + ") order by ib.protocoloEnvio");
        @SuppressWarnings("unchecked")
        List<Object[]> list = query.getResultList();

        Long[] condicao0789 = { 0L, 7L, 8L, 9L };
        List<Long> listCondicao = new ArrayList<>();
        listCondicao.addAll(Arrays.asList(condicao0789));

        if (condicao == PedidoLicenciamentoImportacao.LIQUIDACAO) {
            listCondicao.add(4L);
        }

        if (condicao == PedidoLicenciamentoImportacao.CANCELAMENTO) {
            listCondicao.clear();
            listCondicao.add(5L);
        }

        HashMap<Long, Long> result = new HashMap<>();
        Long ultimoProtocolo;
        for (Object[] object : list) {
            Long protocolo = ((Long) object[0]);
            Long status = ((Long) object[1]);

            ultimoProtocolo = protocolo;

            if (result.get(ultimoProtocolo) == null) {
                result.put(protocolo, 1L);
            }

            if (!ArrayUtils.contains(listCondicao.toArray(), status)) {
                result.put(protocolo, 0L);

            }

            logger.info("OPAAA: " + protocolo + " - " + result.get(ultimoProtocolo) + " - " + status);
        }
        return result;
    }

    @Override
    @Transactional
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void atualizarStatusInfoBasica(TipoDebitoRetornoVO retorno, InformacaoBasica informacaoBasica) {
        logger.info("AtualizacaoStatusInfoBasica:tipoDebito " + retorno.getTipoDebitoRetorno());
        //SUCESSO = 1
        if (retorno.getTipoDebitoRetorno() == TIPO_DEBITO_RETORNO_SUCESSO) {
            logger.info("AtualizacaoStatusInfoBasica: entrou: TIPO_DEBITO_RETORNO_SUCESSO");

            if (informacaoBasica.getStatusDebito() != STATUS_DEBITO_GERADO_CANCELADO && informacaoBasica.getStatusDebito() != STATUS_DEBITO_GERADO_PAGO) {
                if (informacaoBasica.getControleCobrancaTcif() == 0) {
                    informacaoBasica.setStatusDebito(2L);
                } else if (informacaoBasica.getControleCobrancaTcif() == 1) {
                    informacaoBasica.setStatusDebito(7L);
                } else if (informacaoBasica.getControleCobrancaTcif() == 2) {
                    informacaoBasica.setStatusDebito(8L);
                }
            }
            informacaoBasica.setAnoDebito(retorno.getAnoDebito());
            informacaoBasica.setNumeroDebito(retorno.getNumero());
            informacaoBasica.setDataVencimento(retorno.getDataVencimento());
            persistenciaInfoBasica.gravar(informacaoBasica);
            //ERRO = 0  
        } else if (retorno.getTipoDebitoRetorno() == TIPO_DEBITO_RETORNO_ERRO) {
            logger.info("AtualizacaoStatusInfoBasica: entrou: TIPO_DEBITO_RETORNO_ERRO");
            EnvioLog envioLog = new EnvioLog();

            envioLog.setInscricaoSuframa(informacaoBasica.getInscricaoSuframa());
            envioLog.setNumeroPLI(informacaoBasica.getNumeroPLI());
            envioLog.setProtocoloEnvio(informacaoBasica.getProtocoloEnvio());
            envioLog.setCodTipoLog(LOG_ERRO_ESTRUTURA);
            envioLog.setMsgLog(retorno.getMensagem());
            envioLog.setDtCadastro(new Date());

            envioLogNegocio.salvar(envioLog);

            informacaoBasica.setStatusDebito(6L);
            persistenciaInfoBasica.gravar(informacaoBasica);
            //EXISTENTE = 2 
        } else if (retorno.getTipoDebitoRetorno() == TIPO_DEBITO_RETORNO_EXISTENTE) {
            logger.info("AtualizacaoStatusInfoBasica: entrou: TIPO_DEBITO_RETORNO_EXISTENTE");

            EnvioLog envioLog = new EnvioLog();

            envioLog.setInscricaoSuframa(informacaoBasica.getInscricaoSuframa());
            envioLog.setNumeroPLI(informacaoBasica.getNumeroPLI());
            envioLog.setProtocoloEnvio(informacaoBasica.getProtocoloEnvio());
            envioLog.setCodTipoLog(LOG_DEBITO_EXISTENTE);
            envioLog.setMsgLog(retorno.getMensagem());
            envioLog.setDtCadastro(new Date());

            envioLogNegocio.salvar(envioLog);

            checaStatusDebito(retorno, informacaoBasica);
            informacaoBasica.setAnoDebito(retorno.getAnoDebito());
            informacaoBasica.setNumeroDebito(retorno.getNumero());
            informacaoBasica.setDataVencimento(retorno.getDataVencimento());
            persistenciaInfoBasica.gravar(informacaoBasica);

        } else if (retorno.getTipoDebitoRetorno() == TIPO_DEBITO_AUTORIZADO) {
            logger.info("AtualizacaoStatusInfoBasica: entrou: TIPO_DEBITO_AUTORIZADO");

            if (informacaoBasica.getStatusDebito() != STATUS_DEBITO_GERADO_CANCELADO && informacaoBasica.getStatusDebito() != STATUS_DEBITO_GERADO_PAGO) {
                if (retorno.getTipoDebitoRetorno() == 3) {
                    informacaoBasica.setStatusDebito(9L);
                }
            }

            informacaoBasica.setAnoDebito(retorno.getAnoDebito());
            informacaoBasica.setNumeroDebito(retorno.getNumero());
            informacaoBasica.setDataVencimento(retorno.getDataVencimento());
            informacaoBasica.setStatusMsgDebito(retorno.getMensagem());
            persistenciaInfoBasica.gravar(informacaoBasica);

        }
    }

    private void checaStatusDebito(TipoDebitoRetornoVO retorno, InformacaoBasica informacaoBasica) {
        if (informacaoBasica.getStatusDebito() != STATUS_DEBITO_GERADO_CANCELADO && informacaoBasica.getStatusDebito() != STATUS_DEBITO_GERADO_PAGO) {
            if (informacaoBasica.getControleCobrancaTcif() == 0) {
                informacaoBasica.setStatusDebito(2L);
            } else if (informacaoBasica.getControleCobrancaTcif() == 1) {
                informacaoBasica.setStatusDebito(7L);
            } else if (informacaoBasica.getControleCobrancaTcif() == 2) {
                informacaoBasica.setStatusDebito(8L);
            } else {
                informacaoBasica.setStatusDebito(9L);
            }

        }
    }
}

package br.gov.suframa.pme.recebimentosac.business.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.gov.suframa.pme.recebimentosac.business.EnvioLogNegocio;
import br.gov.suframa.pme.recebimentosac.model.EnvioLog;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class EnvioLogNegocioImpl extends NegocioBase implements EnvioLogNegocio {

    @EJB
    private PersistenciaFactory<Long, EnvioLog> persistenciaEnvioLog;

    @Override
    public EnvioLog salvar(EnvioLog envioLog) {

        EnvioLog log = persistenciaEnvioLog.gravar(envioLog);

        return log;
    }

}

package br.gov.suframa.pme.recebimentosac.business.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.gov.suframa.pme.recebimentosac.business.PLINegocio;
import br.gov.suframa.pme.recebimentosac.model.PedidoLicenciamentoImportacao;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class PLINegocioImpl extends NegocioBase implements PLINegocio {

    @EJB
    private PersistenciaFactory<Long, PedidoLicenciamentoImportacao> persistenciaPLI;

    @Override
    @Transactional
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PedidoLicenciamentoImportacao atualizarStatusPLI(Long id, Long status, Long processado) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<PedidoLicenciamentoImportacao> query = getCriteriaQuery(PedidoLicenciamentoImportacao.class);
        Root<PedidoLicenciamentoImportacao> from = query.from(PedidoLicenciamentoImportacao.class);
        query.where(cb.equal(from.get("pliId"), id), cb.equal(from.get("processado"), processado));

        PedidoLicenciamentoImportacao pli = !getEntityManager().createQuery(query).getResultList().isEmpty() ? getEntityManager().createQuery(query).getSingleResult() : null;

        if (pli != null) {
            pli.setProcessado(status);
            return persistenciaPLI.gravar(pli);
        }

        return pli;
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistenciaPLI.getEntityManager();
    }
}

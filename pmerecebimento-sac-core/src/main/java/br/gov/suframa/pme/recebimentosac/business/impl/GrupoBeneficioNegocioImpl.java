package br.gov.suframa.pme.recebimentosac.business.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.gov.suframa.pme.recebimentosac.business.GrupoBeneficioNegocio;
import br.gov.suframa.pme.recebimentosac.model.GrupoBeneficio;
import br.gov.suframa.pme.recebimentosac.persistence.impl.PersistenciaFactory;
import br.gov.suframa.poc.business.NegocioBase;

@Stateless
public class GrupoBeneficioNegocioImpl extends NegocioBase implements GrupoBeneficioNegocio {

    @EJB
    private PersistenciaFactory<Long, GrupoBeneficio> persistencia;

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public GrupoBeneficio consultarGrupoBeneficioPorId(Long id) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<GrupoBeneficio> query = getCriteriaQuery(GrupoBeneficio.class);
        Root<GrupoBeneficio> from = query.from(GrupoBeneficio.class);
        query.where(cb.equal(from.get("id"), id));

        GrupoBeneficio grupo = getEntityManager().createQuery(query).getResultList().size() > 0 ? getEntityManager().createQuery(query).getSingleResult() : null;

        return grupo;
    }

    @Override
    protected EntityManager getEntityManager() {
        return persistencia.getEntityManager();
    }
}

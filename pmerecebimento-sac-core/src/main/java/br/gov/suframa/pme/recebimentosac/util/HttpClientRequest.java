package br.gov.suframa.pme.recebimentosac.util;

import javax.inject.Inject;

import br.gov.suframa.poc.cdi.annotation.SystemProperty;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class HttpClientRequest {

    private static final String HTTP_ERROR = "Failed : HTTP error code : ";
    private static final int HTTP_CODE_SUCCESS = 200;

    @Inject
    @SystemProperty("pme.sac.login")
    private String login;

    @Inject
    @SystemProperty("pme.sac.senha")
    private String senha;

    public String get(String url) {

        Client client = Client.create();

        client.addFilter(new HTTPBasicAuthFilter(login, senha));

        WebResource webResource = client.resource(url);

        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

        if (response.getStatus() != HTTP_CODE_SUCCESS) {
            return (HTTP_ERROR + response.getStatus());
        } else {
            return response.getEntity(String.class);
        }
    }

    public String post(String url, String obj) {

        Client client = Client.create();

        client.addFilter(new HTTPBasicAuthFilter(login, senha));

        WebResource webResource = client.resource(url);
        ClientResponse response = webResource.type("application/json").post(ClientResponse.class, obj);

        if (response.getStatus() != HTTP_CODE_SUCCESS) {
            return (HTTP_ERROR + response.getStatus());
        } else {
            return response.getEntity(String.class);
        }

    }

}
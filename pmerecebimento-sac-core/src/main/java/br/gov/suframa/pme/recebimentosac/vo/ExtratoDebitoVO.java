package br.gov.suframa.pme.recebimentosac.vo;

import java.util.List;

import br.gov.suframa.pme.recebimentosac.model.InformacaoBasica;
import br.gov.suframa.pme.recebimentosac.util.DateUtil;
import br.gov.suframa.poc.vo.BaseVO;

public class ExtratoDebitoVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String numeroPLI;
    private String valorMercadoriaEmReal;
    private String valorTCIFBasePLI;
    private String limitadorPLI;
    private String valorTCIFRealPLI;
    private String dataCotacaoDolar;
    private String taxaDolar;
    private List<NCMVO> listaNCM;

    public ExtratoDebitoVO() {

    }

    public ExtratoDebitoVO(InformacaoBasica info, String taxaDolar, List<NCMVO> listaNCM) {
        this.numeroPLI = info.getNumeroPLI();
        this.valorMercadoriaEmReal = info.getValorTotalMercadoria() != null ? info.getValorTotalMercadoria().toString() : null;
        this.valorTCIFBasePLI = info.getValorBaseFatoGerador() != null ? info.getValorBaseFatoGerador().toString() : null;
        this.limitadorPLI = info.getPercentualLimitadorPLI() != null ? info.getPercentualLimitadorPLI().toString() : null;
        this.valorTCIFRealPLI = info.getValorPreviPLI() != null ? info.getValorPreviPLI().toString() : null;
        this.dataCotacaoDolar = DateUtil.convertFormatBr(info.getDataEnvio());
        this.taxaDolar = taxaDolar;
        this.listaNCM = listaNCM;
    }

    public String getNumeroPLI() {
        return numeroPLI;
    }

    public void setNumeroPLI(String numeroPLI) {
        this.numeroPLI = numeroPLI;
    }

    public String getValorMercadoriaEmReal() {
        return valorMercadoriaEmReal;
    }

    public void setValorMercadoriaEmReal(String valorMercadoriaEmReal) {
        this.valorMercadoriaEmReal = valorMercadoriaEmReal;
    }

    public String getValorTCIFBasePLI() {
        return valorTCIFBasePLI;
    }

    public void setValorTCIFBasePLI(String valorTCIFBasePLI) {
        this.valorTCIFBasePLI = valorTCIFBasePLI;
    }

    public String getLimitadorPLI() {
        return limitadorPLI;
    }

    public void setLimitadorPLI(String limitadorPLI) {
        this.limitadorPLI = limitadorPLI;
    }

    public String getValorTCIFRealPLI() {
        return valorTCIFRealPLI;
    }

    public void setValorTCIFRealPLI(String valorTCIFRealPLI) {
        this.valorTCIFRealPLI = valorTCIFRealPLI;
    }

    public String getDataCotacaoDolar() {
        return dataCotacaoDolar;
    }

    public void setDataCotacaoDolar(String dataCotacaoDolar) {
        this.dataCotacaoDolar = dataCotacaoDolar;
    }

    public String getTaxaDolar() {
        return taxaDolar;
    }

    public void setTaxaDolar(String taxaDolar) {
        this.taxaDolar = taxaDolar;
    }

    public List<NCMVO> getListaNCM() {
        return listaNCM;
    }

    public void setListaNCM(List<NCMVO> listaNCM) {
        this.listaNCM = listaNCM;
    }
}

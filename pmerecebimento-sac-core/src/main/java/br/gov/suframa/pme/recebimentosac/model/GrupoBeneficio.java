package br.gov.suframa.pme.recebimentosac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.suframa.poc.model.EntidadeBase;

@Entity
@Table(name = "JUR_GRUPO_BENEFICIO")
public class GrupoBeneficio extends EntidadeBase<Long> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "GBE_ID")
    private Long id;

    @Column(name = "GBE_REG_ID")
    private Long idRegulamento;

    @Column(name = "GBE_DS")
    private String descricao;

    @Column(name = "GBE_DH_CADASTRO")
    private Date dataCadastro;

    public GrupoBeneficio() {
        // TODO Auto-generated constructor stub
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdRegulamento() {
        return idRegulamento;
    }

    public void setIdRegulamento(Long idRegulamento) {
        this.idRegulamento = idRegulamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}

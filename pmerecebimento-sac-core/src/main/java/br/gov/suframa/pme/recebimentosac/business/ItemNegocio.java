package br.gov.suframa.pme.recebimentosac.business;

import java.util.List;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.Item;

@Local
public interface ItemNegocio {

    List<Item> consultarItemDeMercadoria(Long inscricaoSuframa, String numeroPLI, String codigoNCM, String codigoProduto, String codigoTipo, String codigoModelo, Long nuMercadoria);

}

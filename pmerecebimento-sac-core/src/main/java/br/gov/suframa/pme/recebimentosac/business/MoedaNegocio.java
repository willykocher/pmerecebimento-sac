package br.gov.suframa.pme.recebimentosac.business;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.Moeda;

@Local
public interface MoedaNegocio {
    Moeda consultarMoeda(Long codigoMoeda);
}

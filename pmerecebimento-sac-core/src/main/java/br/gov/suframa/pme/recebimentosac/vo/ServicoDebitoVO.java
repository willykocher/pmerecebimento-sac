package br.gov.suframa.pme.recebimentosac.vo;

import br.gov.suframa.poc.vo.BaseVO;

public class ServicoDebitoVO extends BaseVO {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Long codigo;

    public ServicoDebitoVO() {
        // TODO Auto-generated constructor stub
    }

    public ServicoDebitoVO(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

}

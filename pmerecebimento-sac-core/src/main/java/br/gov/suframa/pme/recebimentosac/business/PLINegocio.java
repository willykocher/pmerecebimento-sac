package br.gov.suframa.pme.recebimentosac.business;

import javax.ejb.Local;

import br.gov.suframa.pme.recebimentosac.model.PedidoLicenciamentoImportacao;

@Local
public interface PLINegocio {

    PedidoLicenciamentoImportacao atualizarStatusPLI(Long id, Long status, Long processado);
}

package br.gov.suframa.pme.recebimentosac.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public DateUtil() {
        // TODO Auto-generated constructor stub
    }

    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    public static String convertFormatBr(Date date) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        return sdfDate.format(date);
    }

    public static String convertFormatBrHora(Date date) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        return sdfDate.format(date);
    }

}
